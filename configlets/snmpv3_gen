import binascii
import sys
import hashlib
import cvp
from itertools import repeat

from cvplibrary import Form, CVPGlobalVariables, GlobalVariableNames

vars = {
  "snmpv3": {
    "mode": "priv",
    "vrf": "mgmt",
    "local-interface": "Management1",
    "snmp_user": {
      "aristasnmp": {
        "mode":"auth",
        "group": "View-Name-1",
        "auth": {
          "password": "abc123ab",
          "cipher": "sha256"
        },
        "priv": {
          "password": "bc123abc",
          "cipher": "aes256"
        }
      },
    },
    "snmp_view": {
      "View-Name-1": {
        "include": ["arista", "interfaces", "interfaces.2.1.2", "system"],
        "exclude": ["arista.3.15.1.1.1.8", "interfaces.2"]
      },
      "View-Name-2": {
        "include": ["arista", "mib2", "snmpv2", "bgp", "interfaces", "system"],
        "exclude": []
      }
    },
    "snmp_group": {
      "snmp-group-1": {
        "read": "View-Name-1",
        "write": "View-Name-1",
        "notify": "View-Name-1"
      },
      "snmp-group-2": {
        "read": "View-Name-2",
        "write": "View-Name-2",
        "notify": "View-Name-2"
      }
    }
  }
}

# Get SNMPv3 EngineID (default to IANA PEN + MAC)
snmp_engineID = Form.getFieldById( 'snmp_engineID' ).getValue()
if not snmp_engineID:
  snmp_engineID = 'f5717f'+("".join(CVPGlobalVariables.getValue(GlobalVariableNames.CVP_MAC).split(":")))+"00"

try:
  snmp_vrf = vars['snmpv3']['vrf']
except:
  snmp_vrf = u'default'

try:
  snmp_locInt = vars['snmpv3']['local-interface']
except:
  snmp_locInt = u'Management1'

try:
  snmp_mode = vars['snmpv3']['mode']
except:
  ## Set default mode to "priv"
  snmp_mode = u'priv'

def hash(bytes, alg):
  if alg == 'aes':
    digest = hashlib.aes(bytes).digest()
  if alg == 'aes256':
    digest = hashlib.aes256(bytes).digest()
  if alg == 'md5':
    digest = hashlib.md5(bytes).digest()
  elif alg == 'sha':
    digest = hashlib.sha1(bytes).digest()
  elif alg == 'sha224':
    digest = hashlib.sha224(bytes).digest()
  elif alg == 'sha256':
    digest = hashlib.sha256(bytes).digest()
  elif alg == 'sha384':
    digest = hashlib.sha384(bytes).digest()
  elif alg == 'sha512':
    digest = hashlib.sha512(bytes).digest()
  #if sys.version_info[0] < 3:
  #  return binascii.hexlify(digest)
  #else:
  #  return digest.hex()
  return digest

def expand(substr, target_len):
    reps = target_len // len(substr) + 1
    repsReturn = "".join(list(repeat(substr, reps)))[:target_len]
    return repsReturn

#try:
print ("snmp-server engineID local %s" %snmp_engineID)
print ("snmp-server vrf %s local-interface %s" %(snmp_vrf,snmp_locInt))
print ("snmp-server vrf %s" %snmp_vrf)

E = bytearray.fromhex(snmp_engineID)
if sys.version_info[0] < 3:
  E = str(E)

for snmpview in vars['snmpv3']['snmp_view']:
  for viewInclude in vars['snmpv3']['snmp_view'][snmpview]['include']:
    print ("snmp-server view %s %s included" %(snmpview,viewInclude))
  for viewExclude in vars['snmpv3']['snmp_view'][snmpview]['exclude']:
    print ("snmp-server view %s %s excluded" %(snmpview,viewExclude))

for snmpgroup in vars['snmpv3']['snmp_group']:
  snmpGroupLine = "snmp-server group "+snmpgroup+" v3 "+snmp_mode

  if vars['snmpv3']['snmp_group'][snmpgroup]['read'] != '':
    snmpGroupLine = snmpGroupLine+' read '+vars['snmpv3']['snmp_group'][snmpgroup]['read']
  if vars['snmpv3']['snmp_group'][snmpgroup]['write'] != '':
    snmpGroupLine = snmpGroupLine+' write '+vars['snmpv3']['snmp_group'][snmpgroup]['write']
  if vars['snmpv3']['snmp_group'][snmpgroup]['notify'] != '':
    snmpGroupLine = snmpGroupLine+' notify '+vars['snmpv3']['snmp_group'][snmpgroup]['notify']
  print ("%s" %snmpGroupLine)
print ("!")

for snmpuser in vars['snmpv3']['snmp_user']:
  if snmp_mode == 'priv':
    snmp_priv = True
  else:
    snmp_priv == False
  auth_pass = expand(vars['snmpv3']['snmp_user'][snmpuser]['auth']['password'], 1048576).encode("utf-8")
  auth_cipher = vars['snmpv3']['snmp_user'][snmpuser]['auth']['cipher']
  auth_hash = hash(auth_pass,auth_cipher)
  auth_joined = hash(b"".join([auth_hash, E, auth_hash]),auth_cipher)
  if sys.version_info[0] < 3:
    auth_joinedHash = binascii.hexlify(auth_joined)
  else:
    auth_joinedHash = auth_joined.hex()
  snmp_group = vars['snmpv3']['snmp_user'][snmpuser]['group']
  if snmp_priv is not False:
    priv_pass = expand(vars['snmpv3']['snmp_user'][snmpuser]['priv']['password'], 1048576).encode("utf-8")
    priv_pass = vars['snmpv3']['snmp_user'][snmpuser]['priv']['password']
    priv_cipher = vars['snmpv3']['snmp_user'][snmpuser]['priv']['cipher']
    ##
    ## NEEDS WORK TO DERIVE ENC PRIV FROM SUPPLIED PASS
    ##   once complete, update print line to reference enc priv, rather than cleartext
    print ("snmp user %s %s v3 auth %s %s priv %s %s" %(snmpuser,snmp_group,auth_cipher,auth_joinedHash,priv_cipher,priv_pass))
  else:
    print ("snmp user %s %s v3 auth %s %s" %(snmpuser,snmp_group,auth_cipher,auth_joinedHash))

