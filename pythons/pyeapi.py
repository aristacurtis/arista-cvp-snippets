#!/usr/bin/python
from __future__ import print_function
# import the client library
from netaddr import IPNetwork
import pyeapi
import sys

# load the conf file and connect to the node
pyeapi.load_config('nodes.conf')
node = pyeapi.connect_to('veos01')


newVrfs = {
        'tenantA':{'rd':'1.2.3.4:65101','description':'tenant VRF A','ipv4_routing':True,'vni':'75003','rt':'65000:101','state':True},
        'tenantB':{'rd':'1.2.3.4:65102','description':'tenant VRF B','ipv4_routing':True,'vni':'75004','rt':'65000:102'},
        'tenantC':{'rd':'1.2.3.4:65103','description':'tenant VRF C','ipv4_routing':True,'vni':'75005','rt':'65000:103'},
        'tenantD':{'rd':'1.2.3.4:65104','description':'tenant VRF D','ipv4_routing':True,'vni':'75006','rt':'65000:104'},
        'tenantE':{'rd':'1.2.3.4:65105','description':'tenant VRF E','ipv4_routing':True,'vni':'75007','rt':'65000:105'},
        'tenantF':{'rd':'1.2.3.4:65106','description':'tenant VRF F','ipv4_routing':True,'vni':'75008','rt':'65000:106','state':True},
        'tenantG':{'rd':'1.2.3.4:65107','description':'tenant VRF G','ipv4_routing':True,'vni':'75009','rt':'65000:107'},
        'tenantH':{'rd':'1.2.3.4:65108','description':'tenant VRF H','ipv4_routing':True,'vni':'75010','rt':'65000:108'},
        'tenantI':{'rd':'1.2.3.4:65109','description':'tenant VRF I','ipv4_routing':True,'vni':'75011','rt':'65000:109'},
        'tenantY':{'rd':'1.2.3.4:65200','description':'tenant VRF Y','ipv4_routing':True,'vni':'75387','rt':'65000:200'}
}

vxlanInterface = node.api('interfaces').get('Vxlan1')
Lo0Interface = node.api('ipinterfaces').get('Loopback0')
#print vars(IPNetwork(Lo0Interface['address']))
bgpConfig = node.api('bgp').get()

##  Check that BGP is configured, interface VX1 exists, is not shutdown, has a source-int, and valid udp source-port

try:
    if vxlanInterface['shutdown'] is False and bgpConfig['shutdown'] is False:
        # Configure L3VNI
        for vrfInstance in newVrfs:
            try:
                if newVrfs[vrfInstance]['state'] is False:
                    # set removal flag
                    tenantState = False
                else:
                    tenantState = True
                    
            except:
                tenantState = False
                pass

            vxlanVrfCmds = ["enable","configure","interface Vxlan1"]
            bgpVrfCmds = ["enable","configure","router bgp "+str(bgpConfig['bgp_as'])]
            rtrGenCmds = ["enable","configure","router general"]
            if tenantState is True:
                print ("Creating VRF %s" %vrfInstance)
                node.api('vrfs').create(vrfInstance)
                node.api('vrfs').set_description(vrfInstance,description=newVrfs[vrfInstance]['description'])
                if newVrfs[vrfInstance]['ipv4_routing'] is not True:
                  node.api('vrfs').set_ipv4_routing(vrfInstance,disable=True)
                else:
                  node.api('vrfs').set_ipv4_routing(vrfInstance)
                #print ("Add API call to configure Vxlan1 VNI for %s" %vrfInstance)
                vxlanVrfCmds.append("  vxlan vrf "+vrfInstance+" vni "+str(newVrfs[vrfInstance]['vni']))
                #print ("Add API call to configure BGP L3VPN/VRF for %s" %vrfInstance)
                bgpVrfCmds.append("  vrf "+vrfInstance)
                bgpVrfCmds.append("  rd "+newVrfs[vrfInstance]['rd'])
                bgpVrfCmds.append("  route-target import evpn "+newVrfs[vrfInstance]['rt'])
                bgpVrfCmds.append("  route-target export evpn "+newVrfs[vrfInstance]['rt'])
                bgpVrfCmds.append("  redistribute connected include leaked")
                bgpVrfCmds.append("  redistribute static include leaked")
                bgpVrfCmds.append("  redistribute bgp leaked route-map DEFAULT-ROUTE-ONLY")
                rtrGenCmds.append("  vrf "+vrfInstance)
                rtrGenCmds.append("  leak routes source-vrf PUBLIC subscribe-policy DEFAULT-ROUTE-ONLY")
                rtrGenCmds.append("  vrf PUBLIC")
                rtrGenCmds.append("  leak routes source-vrf "+vrfInstance+" subscribe-policy NAT")
                print ("! Vxlan Config")
                print (vxlanVrfCmds)
                vxlanVrfResponse = node.run_commands(vxlanVrfCmds)
                #print (vxlanVrfResponse)
                print ("! BGP Config")
                print (bgpVrfCmds)
                print ("! General Config")
                print (rtrGenCmds)
                bgpVrfResponse = node.run_commands(bgpVrfCmds)
                rtrGenResponse = node.run_commands(rtrGenCmds)
                #print (vxlanVrfResponse)
            else:
                print ("Oh, we're going to remove this tenant")
                node.api('vrfs').delete(vrfInstance) 
                vxlanVrfCmds.append("  no vxlan vrf "+vrfInstance+" vni "+str(newVrfs[vrfInstance]['vni']))
                bgpVrfCmds.append("  no vrf "+vrfInstance)
                rtrGenCmds.append("  no vrf "+vrfInstance)
                rtrGenCmds.append("  vrf PUBLIC")
                rtrGenCmds.append("  no leak routes source-vrf "+vrfInstance+" subscribe-policy NAT")
                vxlanVrfResponse = node.run_commands(vxlanVrfCmds)
                bgpVrfResponse = node.run_commands(bgpVrfCmds)
                rtrGenResponse = node.run_commands(rtrGenCmds)

    else:
        raise UserWarning("! Interface Vxlan1 is not enabled")
except UserWarning as e:
    print ("! Warning: %s" %e)
except Exception as e:
    print ("! Exception - vxlanInterface: %s" %e)
print ()
